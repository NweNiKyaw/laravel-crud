-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2019 at 05:14 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog2`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Impedit', '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(2, 'Accusamus', '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(3, 'Hic', '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(4, 'Maiores', '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(5, 'Ut', '2019-03-21 07:57:21', '2019-03-21 07:57:21');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 'Dolorum fugit sint blanditiis praesentium quibusdam facere minima.', 3, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(2, 'Error quisquam sint assumenda aut non.', 5, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(3, 'Minima distinctio sit repellendus consectetur quo quam numquam reiciendis.', 8, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(4, 'Laborum qui totam sed unde.', 6, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(5, 'Magnam voluptatem praesentium et adipisci ipsum nostrum facere.', 2, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(6, 'Blanditiis rerum soluta dicta qui quia saepe earum.', 9, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(7, 'Est quas magni et tenetur consectetur provident suscipit dolore.', 2, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(8, 'Excepturi quam optio dolores modi illum.', 10, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(9, 'Ullam natus ea iusto voluptatem dolores quia.', 2, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(10, 'Vel molestiae omnis sit ut et sequi a in.', 8, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(11, 'Rerum nihil et pariatur voluptatem totam eius.', 9, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(12, 'Illum tempora inventore aut excepturi.', 10, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(13, 'Qui amet sunt delectus error ipsa harum.', 1, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(14, 'Cumque ad assumenda eos est.', 4, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(15, 'Consequuntur tempora quia itaque rerum iste autem sint voluptates.', 8, '2019-03-21 07:57:22', '2019-03-21 07:57:22'),
(16, 'Hic et laboriosam sunt minima repellat.', 5, '2019-03-21 07:57:22', '2019-03-21 07:57:22'),
(17, 'Facilis ratione enim debitis non quia.', 2, '2019-03-21 07:57:22', '2019-03-21 07:57:22'),
(18, 'Alias molestias distinctio maiores exercitationem.', 8, '2019-03-21 07:57:22', '2019-03-21 07:57:22'),
(20, 'Repellendus in possimus repudiandae nisi magnam.', 4, '2019-03-21 07:57:22', '2019-03-21 07:57:22'),
(21, 'some comment', 6, '2019-03-21 09:19:01', '2019-03-21 09:19:01'),
(22, 'Hello', 7, '2019-03-21 09:25:58', '2019-03-21 09:25:58'),
(23, 'Thank you', 7, '2019-03-21 09:26:12', '2019-03-21 09:26:12'),
(24, 'Hello', 1, '2019-03-26 04:30:58', '2019-03-26 04:30:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2014_10_12_000000_create_users_table', 1),
(19, '2014_10_12_100000_create_password_resets_table', 1),
(20, '2019_03_21_040504_create_posts_table', 1),
(21, '2019_03_21_103721_create_categories_table', 1),
(22, '2019_03_21_142434_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Culpa voluptatem ea cumque quia ipsa rerum.', 'Excepturi commodi rerum consequatur non qui. Aliquam totam saepe et. Et est debitis amet cum commodi quam. Reiciendis saepe quidem accusamus nisi.', 3, '2019-03-21 07:57:20', '2019-03-21 07:57:20'),
(2, 'Repellendus voluptatem hic perspiciatis iste et sed qui.', 'Dolor rerum sint nam excepturi eum aut. Et provident excepturi eligendi expedita voluptatum et. Maiores vitae quis enim quis. Et at et impedit eligendi perspiciatis dolor. Sint itaque praesentium non ad id.', 2, '2019-03-21 07:57:20', '2019-03-21 07:57:20'),
(3, 'Non aut et enim unde nemo doloremque.', 'Praesentium facilis sed dicta et. Amet ea ut sed. Sunt vitae ducimus aut animi ad explicabo facere pariatur. Cupiditate amet totam ipsum corporis ut id.', 2, '2019-03-21 07:57:20', '2019-03-21 07:57:20'),
(4, 'Magnam et quis at ad quam est maiores.', 'Recusandae amet pariatur quod consectetur expedita rem et. Optio voluptate quas quo est qui vero.', 4, '2019-03-21 07:57:20', '2019-03-21 07:57:20'),
(5, 'At est laudantium consectetur unde eveniet.', 'Maxime commodi fugit iusto ut esse enim ab. Eum dolor dolor sit omnis similique sequi. Qui libero est autem et est alias. Et sit aut qui delectus dolor. Laboriosam dignissimos doloremque ut provident omnis dolore deleniti.', 5, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(6, 'Et dic', 'Rem dicta sit qui porro. Facere impedit repellat vero iste ipsa. Dolores dolorem quisquam quos et et provident iure labore. Ea ullam fugit quia vel est qui ut.', 1, '2019-03-21 07:57:21', '2019-03-21 08:59:14'),
(7, 'Sed quaerat expedita eos ut et.', 'Labore velit nemo dolores officia adipisci odio assumenda earum. Qui deserunt deleniti distinctio assumenda. Et asperiores eligendi in et inventore. Optio et quia a amet expedita modi.', 2, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(8, 'Provident nihil doloremque in quo ipsam quae ut.', 'Voluptatem ad nemo veniam similique. Explicabo hic modi officia accusantium accusantium itaque. Exercitationem eaque nisi sed totam cum quaerat. Quisquam sapiente et vero. Ipsum error sit odit voluptatem.', 2, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(9, 'Animi aliquid quasi dolor ut ipsam est.', 'Qui sint et ipsam incidunt iure deserunt. Possimus quia est sed modi repellat dolorum officia aut. Vel officiis doloribus qui ratione illo sunt. Natus explicabo modi ut quisquam voluptatem ut harum.', 1, '2019-03-21 07:57:21', '2019-03-21 07:57:21'),
(10, 'Corrupti molestiae adipisci sed cumque et.', 'Natus commodi ea similique cumque magni aut. Ducimus quo reiciendis sint nesciunt iste rerum qui. Commodi repellendus error exercitationem maiores aut laborum ut. Delectus qui voluptas maiores qui fugiat in.', 5, '2019-03-21 07:57:21', '2019-03-21 07:57:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin123@gmail.com', '$2y$10$DyZKXjv.WayV94jX8JCCnu0b0HTYFr3.kk4Y/9WUdO5e.auRFyZJ.', NULL, '2019-03-26 21:40:59', '2019-03-26 21:40:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
