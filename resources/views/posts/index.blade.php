@extends ('layouts.app')

@section ('content')
	<div class="container">
	@if(session('danger'))
		<div class="alert alert-danger">
			{{session('danger')}}
		</div>
	@endif

		@foreach( $posts as $post)
			<div class="panel panel-success">
				<div class="panel-heading">
					<a href="{{url("/posts/view/$post->id")}}"><h4>{{$post -> title}}</h4></a>
					
				</div>
				<div class="panel-body">
					{{$post -> body}}
				</div>
				<div class="panel-footer">
					<b>{{$post->category->name}}</b>;
					{{$post -> created_at->diffForHumans()}},
					{{count($post->comments)}} Comments
				</div>
			</div>
		@endforeach

		{{$posts->links()}}
	</div>
@endsection