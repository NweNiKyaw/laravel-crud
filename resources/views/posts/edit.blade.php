@extends ('layouts.app')

@section ('content')
	<div class="container">
		<h2>Add Post</h2>
		<form method="post">
		{{csrf_field()}}
				<div class="form-group">
					<label>Title</label>
					<input type="text" name="title" class="form-control" value="{{$post -> title}}">
				</div>
				<div class="form-group">
					<label>Body</label>
					<textarea name="body" class="form-control">{{$post->body}}</textarea>
				</div>
				<div class="form-group">
					<label>Category</label>
					<select name="category_id" class="form-control">
						@foreach($categories as $category)
							<option value= "{{$category->id}}">{{$category->name}}</option>
						@endforeach
					</select>
				</div>
			<input type="submit" value="Update Post" class="btn btn-success">
		</form>
	</div>
@endsection